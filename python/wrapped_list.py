class WrappedList(list):
    def __getitem__(self, key):
        
        if isinstance(key, int):
            # try normal list behavior
            try:
                return super(WrappedList, self).__getitem__(key)
            except IndexError:
                pass
            try:
                index = int(key)
                index = index % self.__len__()
                return super(WrappedList, self).__getitem__(index)
            except ValueError:
                raise TypeError
        elif isinstance(key, slice):
            # Normal slicing
            if slice.start < slice.stop or slice.start == slice.stop:
                return super(WrappedList, self).__getitem__(key)
            # Wrap around slicing
            else:
                #print "WRAP"
                first_part = super(WrappedList,
                        self).__getitem__(slice(start=slice.start,
                            stop=self.__len__(),
                            step=slice.step))
                second_part = super(WrappedList,
                        self).__getitem__(slice(stop=slice.stop,
                            step=slice.step))
                return first_part.extend(second_part)
    def __getslice__(self, i, j):
        # Normal slicing
        if i < j or i == j:
            return super(WrappedList, self).__getslice__(i, j)
        # Wrap around slicing
        else:
            #print "WRAP"
            first_part = super(WrappedList,
                    self).__getslice__(i,
                        self.__len__())
            second_part = super(WrappedList,
                    self).__getslice__(0,j)
            #print first_part, second_part
            first_part.extend(second_part)
            return first_part

if __name__ == "__main__":
    from itertools import product
    a = range(5)
    cl = WrappedList(a)
    # Normal
    print "normal"
    for idx, a_val in enumerate(a):
        print a_val, cl[idx]
        assert a_val == cl[idx]
    print "wrap around"
    for idx, a_val in enumerate(a):
        print a_val, cl[idx+len(a)]
        assert a_val == cl[idx+len(a)]

    print "normal slice"
    for i,j in product(a, repeat=2):
        print "idx", i, j
        print a[i:j], cl[i:j], a[i:j] == cl[i:j]
        if i <= j: 
            assert a[i:j] == cl[i:j]

