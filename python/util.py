import math
import json

def make_lane_indexes(gameinfo):
    lane_indexes = {}
    for lane_info in gameinfo["race"]["track"]["lanes"]:
        lane_indexes[lane_info["index"]] = \
        lane_info["distanceFromCenter"]
    return lane_indexes

def fill_pieces(pieces, start_angle, lane_indexes):
    # TODO: Changes angle to radians?
    for piece in pieces:
        if "angle" in piece:
            piece["rad_angle"] = math.radians(piece["angle"])
            piece["sin_rad_angle"] = math.sin(piece["rad_angle"])
            piece["cos_rad_angle"] = math.cos(piece["rad_angle"])
            piece["cur_ful_angle"]=start_angle+piece["angle"]
            start_angle+=piece["angle"]
            def calc_length_for_radius(radius, angle, lane_indexes):
                def calc_length(lane_index, radius=radius, angle=angle,
                        lane_indexes=lane_indexes):
                    # We have to add or substract on which lane are we
                    # currently
                    # adding or substracting depends on the direction
                    # of the angle
                    # curve to the right
                    if angle > 0:
                        # negative distances are on the outside of the curve
                        # positive on the inside
                        lane_diff = -1 * lane_indexes[lane_index]
                    # curve to the left
                    else:
                        # positive distances are on the outside of the curve
                        # negative on the inside
                        lane_diff = lane_indexes[lane_index]
                    radius = radius + lane_diff
                    length = (math.pi*radius*math.fabs(angle))/180.0
                    return length
                return calc_length
            
            def calc_angle_for_length(radius, angle, lane_indexes):
                def calc_angle(lane_index, length, angle=angle, radius=radius,
                        lane_indexes=lane_indexes):
                    # We have to add or substract on which lane are we
                    # currently
                    # adding or substracting depends on the direction
                    # of the angle
                    # curve to the right
                    if angle > 0:
                        # negative distances are on the outside of the curve
                        # positive on the inside
                        lane_diff = -1 * lane_indexes[lane_index]
                    # curve to the left
                    else:
                        # positive distances are on the outside of the curve
                        # negative on the inside
                        lane_diff = lane_indexes[lane_index]
                    radius = radius + lane_diff
                    cur_angle = (180.0 * length) / (math.pi * radius)
                    return math.copysign(cur_angle, angle)
                return calc_angle

            piece["length"] = calc_length_for_radius(piece["radius"],
                    piece["angle"],
                    lane_indexes)
            piece["cur_angle"] = calc_angle_for_length(piece["radius"],
                    piece["angle"],
                    lane_indexes)
        else:
            piece["cur_ful_angle"] = start_angle

def plot(filename, show_plot=True, save_file=True):
    import os.path
    import prettyplotlib as ppl
    import numpy as np

# prettyplotlib imports 
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    from prettyplotlib import brewer2mpl

    data = np.genfromtxt(filename, delimiter=',', names=True)

    fig, ax = plt.subplots(1)

    x = np.arange(len(data))

    for column_name in data.dtype.names:
        if column_name.startswith("_"):
            continue
        y = data[column_name]

        ppl.plot(ax, x, y, label=column_name, linewidth=0.75)

    ppl.legend(ax)

    if show_plot:
        plt.show()

    if save_file:
        out_filename, ext = os.path.splitext(filename)
        fig.savefig(out_filename + '.png')

class LogType:
    GAME = -1
    NETLOG = 0
    VIZUALIZATION = 1
    TXTLOG = 2


def read_json_data(filename, logtype=LogType.NETLOG):
    import re
    regex = re.compile("^(#[SR]:)")
    file = open(filename, 'rb')
    if logtype == LogType.VIZUALIZATION:
        json_data = json.load(file)
        for json_item in json_data:
            yield json_item
    elif logtype == LogType.TXTLOG:
        for line in file:
            if line.strip():
                if line.startswith("#"):
                    line = regex.sub("", line)
                    json_line = json.loads(line)
                    yield json_line
    elif logtype == LogType.NETLOG:
        for line in file:
            if line.strip():
                json_line = json.loads(line)
                yield json_line
    else:
        raise Exception("{0} is an unknown log type!".format(logtype))
