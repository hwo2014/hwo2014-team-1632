import json
import math
from dashboard import Dashboard
from collections import defaultdict
import csv
from util import plot, read_json_data, LogType

filename = "../usa473.txt"
logtype = LogType.TXTLOG
csv_filename_path = "pid_seaki.csv"
bot_name = "Valyrian speed"
#bot_name = "SE-Aki"


d = defaultdict(dict)
if not logtype == LogType.VIZUALIZATION:
    d[0]["throttle"] = 0
    d[0]["_diff"] = 0
    d[0]["fake_speed"] = 0
gametick = 0
my_dash = None
data_items = read_json_data(filename, viz=vizualization, log=is_log)
for json_line in data_items:
    if json_line["msgType"] == 'carPositions' or \
            json_line["msgType"] == 'fullCarPositions':
        data = json_line["data"]
        tick = json_line.get("gameTick", 0)
        my_dash.new_car_position(data, tick, logtype=logtype)

        d[tick]["speed"] = my_dash.cur_info.speed
        #d[tick]["v_speed"] = my_dash.cur_info.v_speed
        #d[tick]["x_speed"] = my_dash.cur_info.speed_x
        #d[tick]["y_speed"] = my_dash.cur_info.speed_y
        d[tick]["speed_limit"] = my_dash.get_current_speed_limit()
        d[tick]["slip_angle"] = my_dash.cur_info.slip_angle
        d[tick]["accel"] = my_dash.cur_info.accel
        gametick = tick

        my_dash.print_info()
    elif json_line["msgType"] == 'lapFinished':
        print "\nLAP FINISHED"
        data = json_line["data"]
        print data
        my_dash.lap_finished(data)
        print "\n"

    elif json_line["msgType"] == 'spawn':
        my_dash.spawned()
        
    elif json_line["msgType"] == 'crash':
        my_dash.crashed()
        

    elif json_line["msgType"] == 'throttle':
        data = json_line["data"]
        tick = json_line.get("gameTick", 0)
        #tick = gametick
        d[tick]["throttle"] = data
        if tick <= 2:
            fspeed = 0
        else:
            prev_tick = tick-1
            if "fake_speed" in d[prev_tick]:
                prev_speed = d[prev_tick]["fake_speed"]
                prev_throttle = d[prev_tick]["throttle"]
            else:
                prev_speed = d[prev_tick-1]["fake_speed"]
                prev_throttle = d[prev_tick-1]["throttle"]
            fspeed = prev_speed+((prev_throttle*10)-prev_speed)*acc_factor
        d[tick]["fake_speed"] = fspeed
        d[tick]["_diff"] = round(fspeed - d[tick]["speed"], 5)
        my_dash.set_throttle(data)

    elif json_line["msgType"] == 'crash':
        print "!!!CRASH!!!!"
        my_dash.calculate_speed_limits() #mu_friction=mu_data[next_mu])
        next_mu+=1
    elif json_line["msgType"] == 'gameInit':
        gameinfo = json_line["data"]
        my_dash = Dashboard(gameinfo, bot_name)
        mu_friction = gameinfo.get("mu_friction")
        if mu_friction is not None:
            print "Setting friction to {0}".format(mu_friction)
            my_dash.calculate_speed_limits(mu_friction=mu_friction)
        else:
            my_dash.calculate_speed_limits()


my_data = []
max_tick = max(d.keys())
for tick_num in range(max_tick):
    my_data.append(d[tick_num])
with open(csv_filename_path, "wb") as outfile:
    keys = d[0].keys()
    writer = csv.DictWriter(outfile, fieldnames=keys,
            quoting=csv.QUOTE_NONNUMERIC)
    writer.writeheader()
    writer.writerows(my_data)
plot(csv_filename_path)
