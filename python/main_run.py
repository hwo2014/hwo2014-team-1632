#!/usr/bin/env python2
"""HWO runner

Usage:
    main_run.py <host> [--track=<track> --cars=<cars> --pass=<pass> --bname=<bot_name>]
    main_run.py --help

Options:
    -h --help         Show this screen
    --versioni        Show version
    --track=<track>   Choose track [default: keimola]
    --cars=<cars>     Number of cars 1..8 [default: 1]
    --pass=<pass>     Server password 
    --bnam=<bot_name> Bot name [default: Valyrian speed]

Tracks:
    keimola, germany, usa, france, elaeintarha, imola, england, suzuka,

Servers:
    hakkinen, senna, webber, prost


"""
from docopt import docopt
from main import NoobBot
import socket

if __name__ == "__main__":
    import sys
    import os
    botkey = "H6yqbIxqQi2vcA"
    port = 8091
    servers = [
            "hakkinen",
            "senna",
            "webber",
            "prost"
            ]
    tracks = [
            "keimola",
            "germany",
            "usa",
            "france",
            "elaeintarha",
            "imola",
            "england",
            "suzuka",
            ]
    arguments = docopt(__doc__, version='0.5')
    #print arguments
    errors = False
    if arguments["<host>"] in servers:
        host = arguments["<host>"]+".helloworldopen.com"
    elif arguments["<host>"] == "localhost":
        host = "localhost"
    else:
        errors = True
        print "Invalid host"

    if arguments["--track"] is None:
        track = None
    elif arguments["--track"] in tracks:
        track = arguments["--track"]
    else:
        errors = True
        track = arguments["--track"]
        print "Invalid track: {0}. Valid tracks: {1}".format(track, ", ".join(tracks))

    if arguments["--cars"] is None:
        cars = 1
    elif 1 <= int(arguments["--cars"]) <= 8 :
        cars = int(arguments["--cars"])
    else:
        errors = True
        cars = int(arguments["--cars"])
        print "Invalid number of cars: {0}".format(arguments["--cars"])

    if arguments["--pass"] is None:
        password = None
    else:
        password = arguments["--pass"]
    
    if arguments["--bname"] is None:
        bot_name = "Valyrian speed"
    else:
        bot_name = arguments["--bname"]

    print("Connecting with parameters:")
    print "Host:{0} track:{1} cars:{2} pass:'{3}' bot:{4}".format(
            host, track, cars, password, bot_name)

    if errors:
        sys.exit(1)

    a = os.popen("git rev-parse HEAD").read()
    print "COMMIT ID:", a
        
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    bot = NoobBot(s, bot_name, botkey, track, cars, password)
    bot.run()
