import json
import socket
import sys
import math
from dashboard import Dashboard, ChangeLane, Direction
from pid import PID

P_DEBUG = True


class NoobBot(object):


    def __init__(self, socket, name, key, track=None, cars=1, password=None):
        self.socket = socket
        self.name = name
        self.key = key
        self.throttle_value = 0.7
        self.last_gametick = None
        self.send_switch_on = {}
        self.current_lap = 0
        self.game_started = False
        self.crashed = False
        self.cur_turbo = None
        self.next_turbo = None
        self.switch_on = {}
        self.dashboard = None
        self.in_turbo = False
        self.trackname = track
        self.num_cars = cars
        self.password = password


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        if P_DEBUG:
            print "#S: ", msg
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.dashboard.set_throttle(throttle)
        self.send(json.dumps({"msgType": "throttle", "data": throttle,
            "gameTick": self.last_gametick}))
        #self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def create_race(self, trackname, password=None, car_count=1):
        msg_data = {
            "botId": {
                "name": self.name,
                "key": self.key
                },
            "trackName": trackname,
            "carCount": car_count
            }
        if password is not None:
            msg_data["password"] = password
        self.msg("createRace", msg_data)

    def join_race(self, trackname, password=None, car_count=1):
        msg_data = {
            "botId": {
                "name": self.name,
                "key": self.key
                },
            "trackName": trackname,
            "carCount": car_count
            }
        if password is not None:
            msg_data["password"] = password
        print "Joining race", msg_data
        self.msg("joinRace", msg_data)


    def run(self):
        #self.create_race(trackname, password=password, car_count=1)
        if self.trackname != None:
            self.join_race(self.trackname, password=self.password,
                    car_count=self.num_cars)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        #self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.game_started = True
        self.throttle(1.0)
        self.switch_on = self.dashboard.get_switching_for_current_lane()
        #print "SWITCHING:"
        #print self.switch_on
        #self.ping()

    def on_game_init(self, data):
        print("Game init")
        if self.dashboard is None:
            self.dashboard = Dashboard(data, self.name)
            self.dashboard.calculate_speed_limits(mu_friction=0.0485005421) #mu_friction=0.0445) #mu_friction=0.05095421)
        else:
            print "Updating gameinit"
            self.dashboard.new_gameinit(data)
            self.last_gametick = None
            self.game_started = False
            self.crashed = False
            self.cur_turbo = None
        self.pid = PID()
        self.in_turbo = False
        # TODO: Should check how it is with ticks and starting. First
        # carPositions doesn't have a tick. then is gamestart with tick 0 then
        # car positions with tick 1
        #self.last_gametick = 0
        self.current_lap = 0
        print "\n"+self.dashboard.trackname+"\n"
        if self.dashboard.qualifying:
            print "QUALIFYING"
        if self.dashboard.quick_race is True:
            print "QUICK RACE"
        elif self.dashboard.quick_race is False:
            print "REAL RACE"
        if self.dashboard.gather_data:
            print "WE SHOULD GET SOME DATA!!"

    def switch_left(self, piece_index):
        return self.switch(piece_index, "Left")

    def switch_right(self, piece_index):
        return self.switch(piece_index, "Right")

    def switch(self, change_lane):
        if change_lane.index in self.send_switch_on:
            return False
        else:
            print "Switching to %s on %d" % (change_lane.direction,
                    change_lane.index)
            self.msg("switchLane", change_lane.direction)
            if change_lane.once:
                self.send_switch_on[change_lane.index] = True
            return True

    def on_car_positions(self, data):
        #print "on_car_positions"
        self.dashboard.new_car_position(data, self.last_gametick)
        if self.last_gametick is None:
            print "Gametick None, returning"
            return
        has_switched = False
        #distance_to = self.dashboard.distance_to_apex(self.dashboard.coming_apex)
        #stop_length, ticks = self.dashboard.distance_to_slowdown(5)
        #print distance_to, stop_length, ticks
        cur_speed_limit = \
                self.dashboard.get_current_speed_limit(no_limit=15)
        # TODO: set speed only if different from previous
        self.pid.setPoint(cur_speed_limit)

        if self.dashboard.cur_info.distance_to_end <= self.dashboard.cur_info.speed:
            #TODO: this can happen more then once

        #if self.dashboard.cur_info.is_this_different_piece():
            # Added for overtaking calculations
            self.switch_on = \
                self.dashboard.get_switching_for_current_lane(self.switch_on["start"]["lane"])
            #print "different_piece", self.dashboard.cur_info.piece_index
            switching_data = \
                    self.switch_on["switching"].get(self.dashboard.cur_info.piece_index)
            if switching_data is not None:
                has_switched = self.switch(switching_data)
            print "Setting speed to:", cur_speed_limit

        # throttle 0 is used for calculating acceleration factor and max_speed
        if self.last_gametick == 4 and self.dashboard.gather_data:
            print "Gathering data for speed"
            self.throttle_value = 0
        else:
            self.throttle_value = self.pid.update(self.dashboard.cur_info.speed)

        self.throttle_value = max(self.throttle_value, 0.0)
        self.throttle_value = min(self.throttle_value, 1.0)
        # Currently we only use Turbo in last straight over the finish 
        if not has_switched and self.cur_turbo is not None:
            #print "HAS TURBO"
            if self.throttle_value != 0.0 and \
                    (self.dashboard.cur_info.piece_index>=(self.dashboard.last_turn_index+1)):
                            #and self.last_lap):
                #self.dashboard.get_current_speed_limit() >= 11:
                if self.last_lap or (self.dashboard.trackname != "Imola" and
                        not self.last_lap):
                    print "WANTING TURBO:", self.cur_turbo
                    self.msg("turbo", "Winter is coming!")
                    has_switched = True
            #elif self.dashboard.cur_info.piece_index==(self.dashboard.last_turn_index+1):
                #cur_turbo = self.turbos.pop(0)
                #print "TURBO YEEEHAH!", cur_turbo
                #self.msg("turbo", "Winter is coming!")
            
        if not has_switched:
            self.throttle(self.throttle_value)

    @property
    def last_lap(self):
        """Returns True if this is last lap"""
        return self.dashboard.last_lap

    def on_spawn(self, data):
        if data["name"] != self.name:
            try:
                print("{0} spawned".format(data["name"]))
            except Exception:
                pass
            self.dashboard.spawned(data)
            return
        self.crashed = False
        print "Back in the race baby!"

    def on_crash(self, data):
        if data["name"] != self.name:
            try:
                print("{0} crashed".format(data["name"]))
            except Exception:
                pass
            self.dashboard.crashed(data)
            self.ping()
            return
        print("CRASH: I crashed. 2 Fast 2 Furious!")
        self.crashed = True
        self.cur_turbo = None
        self.in_turbo = False
        self.ping()
        cur_info = self.dashboard.cur_info
        if self.dashboard.is_cur_piece_curved:
            if cur_info.speed < self.dashboard.get_current_speed_limit():
                    print("Crash because speed_limit was to high probably")
                    mu_friction = math.pow(cur_info.speed, 2.0) / \
                            (9.8 * self.dashboard.radius)
                    print "new mu:", mu_friction
                    self.dashboard.calculate_speed_limits(mu_friction=mu_friction)
                    self.pid.setPoint(self.dashboard.get_current_speed_limit())
                    print "Setting speed to:", self.dashboard.get_current_speed_limit()
            else:
                if self.dashboard.cur_info.calc_speed < \
                    self.dashboard.cur_info.speed:
                    print "Bump probably"
                print("Crash because going too fast. Loving the speed ;)")
                # apply brakes sooner
        else:
            print("Crash on straight road. How is this even possible? :S")

    def on_turbo(self, data):
        if self.crashed:
            print "Missed Turbo"
            return
        print "Got turbo factor {turboFactor} for " \
                " {turboDurationMilliseconds} ms".format(**data)
        if self.in_turbo:
            self.next_turbo = data
        else:
            self.cur_turbo = data


    def on_turbo_start(self, data):
        cur_turbo = self.cur_turbo
        # If we started a turbo
        # TODO: Currently doesn't work for other people's turbo
        if cur_turbo is not None and self.dashboard.turbo_start(data,
                cur_turbo):
            #cur_turbo["turboFactor"],
            #cur_turbo["turboDurationTicks"]):
            self.in_turbo = True
            #self.dashboard.turbo_start(cur_turbo["turboFactor"],
                    #cur_turbo["turboDurationTicks"])
            print "TURBO YEEEHAH!", self.cur_turbo
            self.cur_turbo = None

    def on_turbo_end(self, data):
        # If we ended a turbo
        if self.dashboard.turbo_end(data):
            print "TURBO FINISHED"
            self.in_turbo = False
            if self.next_turbo is not None:
                self.cur_turbo = self.next_turbo
                self.next_turbo = None

    def on_dnf(self, data):
        try:
            print "Got DNF:", data
        except Exception:
            pass
        self.dashboard.dnf(data)

    def on_finish(self, data):
        try:
            print "Got Finish:", data
        except Exception:
            pass
        self.dashboard.finish(data)

    def on_lap_finished(self, data):
        # Returns True if we finished a lap
        if not self.dashboard.lap_finished(data):
            return
        self.current_lap = data["lapTime"]["lap"] + 1
        cur_time = data["lapTime"]["millis"]
        print("Lap {0} finished {1}".format(self.current_lap, cur_time/1000.))
        self.switch_on = self.dashboard.get_switching_for_current_lane()

    def on_game_end(self, data):
        print("Race ended")
        self.game_started = False
        self.ping()

    def on_error(self, data):
        try:
            print("Error: {0}".format(data))
        except Exception:
            pass
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'lapFinished': self.on_lap_finished,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'turboAvailable': self.on_turbo,
            'dnf': self.on_dnf,
            'finish': self.on_finish
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            if P_DEBUG:
                print "#R: ", line,
            msg_type, data = msg['msgType'], msg['data']
            if "gameTick" in msg:
                self.last_gametick = msg["gameTick"]
            else:
                self.last_gametick = None
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                #self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
