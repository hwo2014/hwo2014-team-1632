import math
from util import fill_pieces, make_lane_indexes, LogType
import sys
from itertools import tee, izip
from operator import itemgetter
import re
from collections import namedtuple, defaultdict
import networkx as nx
from wrapped_list import WrappedList
#import numpy as np
#import scipy as sp
#from scipy.interpolate import interp1d, UnivariateSpline


ChangeLane = namedtuple('ChangeLane', ['index', 'direction', 'once'])

class Direction:
    LEFT = "Left"
    RIGHT = "Right"

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return izip(a, b)

class CarDimensions(object):

    def __init__(self, dimensions_json):
        self.width = float(dimensions_json["width"])
        self.length = float(dimensions_json["length"])
        self.guide_flag_pos = float(dimensions_json["guideFlagPosition"])
        self.car_dist_to_end = self.length - self.guide_flag_pos


class Dashboard(object):

    def __init__(self, gameinit_data, bot_name, dummy=False, color=None):
        self.name = bot_name
        self.gameinit_data = gameinit_data
        self.pieces = self.gameinit_data["race"]["track"]["pieces"]
        self.lane_indexes = make_lane_indexes(self.gameinit_data)

        track_starting_point = \
        self.gameinit_data["race"]["track"]["startingPoint"]
        self.start_angle = track_starting_point["angle"]
        starting_point_data = \
        self.gameinit_data["race"]["track"]["startingPoint"]["position"]
        self.start_point = (starting_point_data["x"],
                starting_point_data["y"])
        fill_pieces(self.pieces, self.start_angle, self.lane_indexes)
        self.w_pieces = WrappedList(self.pieces)
        self.prev_info = CarInfo(None, self.pieces, self.lane_indexes, 0)
        self.cur_info = self.prev_info
        self.trackname = gameinit_data["race"]["track"]["name"]
        self.path_driven = 0

        self.color = color

        self._update_laps()

        self.current_throttle = 0
        self.mu_friction = None
        self.max_speed = 10.0
        self.original_max_speed = self.max_speed
        self.acc_factor = 0.02
        self.acc_max_speed = None
        self.acc_factor_calculated = False
        self.max_speed_calculated = False
        self.turbo_factor = None
        self.turbo = False
        self.is_crashed = False
        self.is_spawned = False
        self.next_apex = self.cur_apex = None
        self.coming_apex = None
        self.inside_apex = False
        self.graph = None
        self.F_slip = None
        self.cars = {}
        self.dummy = dummy
        self.distance_to_car = (sys.maxint, 0)
        self.getting_closer_x_ticks = 0
        self.num_pieces = len(self.pieces)
        self.has_dnf = False
        self.has_finished = False
        #self.other_car_speeds = defaultdict(lambda: defaultdict(lambda:
            #defaultdict(dict)))
        if not dummy:
            self.make_graph()
            self.calculate_all_switching()
            # This happens in quali and race because number of cars can change
            for car in gameinit_data["race"]["cars"]:
                car_id = car["id"]
                try:
                    print car_id["name"]
                except Exception:
                    pass
                if car_id["name"] == self.name:
                    self.car_dim = CarDimensions(car["dimensions"])
                    continue
                self.cars[car_id["color"]] = Dashboard(gameinit_data, car_id["name"],
                    color=car_id["color"],
                    dummy=True)
                self.cars[car_id["color"]].car_dim = CarDimensions(car["dimensions"])
        else:
            self.acc_factor_calculated = True
            self.max_speed_calculated = True
            try:
                print "Added dummy {0} {1}".format(self.name, self.color)
            except Exception:
                pass

    def _update_laps(self):
        self.laps = self.gameinit_data["race"]["raceSession"].get("laps")
        self.quick_race = \
            self.gameinit_data["race"]["raceSession"].get("quickRace", None)
        self.qualifying = self.laps is None
        self.gather_data = self.quick_race == True or self.qualifying
        self.current_lap = 0

    def new_gameinit(self, gameinit_data):
        self.gameinit_data = gameinit_data
        self.prev_info = CarInfo(None, self.pieces, self.lane_indexes, 0)
        self.cur_info = self.prev_info
        self.path_driven = 0
        self.current_throttle = 0
        self.turbo_factor = None
        self.turbo = False
        self.is_crashed = False
        self.is_spawned = False
        self.next_apex = self.cur_apex = None
        self.coming_apex = None
        self.inside_apex = False
        self._update_laps()
        print "MAX_SPEED:", self.max_speed
        print "ACC_FACTOR:", self.acc_factor


    def make_graph(self):
        """Makes graph of a route with all switches and lengths"""
        DG = nx.DiGraph()
        lane_indexes = self.lane_indexes
        num_pieces = len(self.pieces)
        for index, piece in enumerate(self.pieces):
            angle = piece.get("angle", None)
            if "switch" in piece:
                if angle is None:
                    length = piece["length"] *  1.02060272785287 

                for start_lane_index, end_lane_index in pairwise(lane_indexes):
                    if angle is not None:
                        start_length = piece["length"](start_lane_index)
                        end_length = piece["length"](end_lane_index)

                        bigger_length = max(start_length, end_length)

                        length = bigger_length *  0.9828760806865157
                    #graph_start = lane_index * 100 + index
                    #graph_end = (lane_index + 1) * 100 + (index + 1) % num_pieces
                    #print graph_start, graph_end
                    graph_start = "L_%d_%d" % (start_lane_index, index)
                    graph_end = "L_%d_%d" % (end_lane_index, (index + 1) % num_pieces)
                    DG.add_edge(graph_start, graph_end, weight=length, name="switch")
                    
                    graph_start = "L_%d_%d" % (end_lane_index, index)
                    graph_end = "L_%d_%d" % (start_lane_index, (index + 1) % num_pieces)
                    DG.add_edge(graph_start, graph_end, weight=length, name="switch")
            if angle is not None:
                for lane_index in lane_indexes.keys():
                    #graph_start = lane_index * 100 + index
                    #graph_end = lane_index * 100 + (index + 1) % num_pieces
                    #print graph_start, graph_end
                    graph_start = "L_%d_%d" % (lane_index, index)
                    graph_end = "L_%d_%d" % (lane_index, (index + 1)%num_pieces)
                    length = piece["length"](lane_index)
                    DG.add_edge(graph_start, graph_end, weight=length, name="curve")
            else:
                for lane_index in lane_indexes.keys():
                    #graph_start = lane_index * 100 + index
                    #graph_end = lane_index * 100 + (index + 1) % num_pieces
                    graph_start = "L_%d_%d" % (lane_index, index)
                    graph_end = "L_%d_%d" % (lane_index, (index + 1) % num_pieces)
                    #graph_start = "L_%d_%d" % (lane_index, index)
                    #graph_end = "L_%d_%d" % (lane_index, (index + 1)%num_pieces)
                    DG.add_edge(graph_start, graph_end, weight=piece["length"],
                            name="straight")
        self.graph = DG

    def calculate_switching_path(self, start_lane, end_lane, end_index,
            start_index=0, parameter='weight'):
        """Calculates shortes path according to length betwen postion on lane x
        and index n and lane y and index m"""
        regex = re.compile("L_(?P<lane>\d+)_(?P<index>\d+)")
        #print "cs", start_lane, start_index
        graph_start = "L_%d_%d" % (start_lane, start_index)
        graph_end = "L_%d_%d" % (end_lane, end_index)

        print graph_start, "->",  graph_end, parameter
        sp = nx.shortest_path(self.graph, source=graph_start, target=graph_end,
                weight=parameter)
        length = nx.shortest_path_length(self.graph, source=graph_start,
                target=graph_end, weight=parameter)
        print length
        data = {
                'start': {
                    'lane': start_lane,
                    'index': start_index
                    },
                'end': {
                    'lane': end_lane,
                    'index': end_index
                    },
                'length': length
                }

        switching = {}


        for node in sp:
            r = regex.match(node)
            if r:
                gdict = r.groupdict()
                cur_lane = int(gdict["lane"])
                # Switch is noticed only at the end of a switch that's why - 1
                switch_index = int(gdict["index"]) - 1
                if cur_lane != start_lane:
                    if cur_lane > start_lane:
                        direction = Direction.RIGHT
                    else:
                        direction = Direction.LEFT
                    
                    #print "SWITCH at ", switch_index, direction
                    switching[switch_index - 1] = ChangeLane(
                            switch_index,
                            direction,
                            once=False)
                    start_lane = cur_lane
        data["switching"] = switching
        return data

    def calculate_all_switching(self, parameter='weight'):
        """Calculates all shortest paths between all combinations of lanes """
        start_lane_paths = defaultdict(list)
        end_lane_paths = defaultdict(list)
        lane_keys = self.lane_indexes.keys()
        num_pieces = len(self.pieces)
        if parameter == 'time':
            # Update graph
            for _from, _to, _data in self.graph.edges(data=True):
                speed = _data.get("car_speed", None)
                if speed is None:
                    speed = _data.get("v_max")
                    self.graph[_from][_to]["time"] = _data["weight"]/speed
                else:
                    speed = 0.1
                    self.graph[_from][_to]["time"] = _data["weight"]/speed
                    print _from, _to, _data["name"], self.graph[_from][_to]["time"]
                #TODO: lower car speed up to next switch?
        #print lane_keys
        #import ipdb; ipdb.set_trace()
        for start_lane in lane_keys:
            for end_lane in lane_keys:
                #print start_lane, "->", end_lane, num_pieces-1
                path = self.calculate_switching_path(start_lane, end_lane,
                        num_pieces-1, parameter=parameter)
                start_lane_paths[start_lane].append(path)
                end_lane_paths[end_lane].append(path)
        for lane_index in start_lane_paths.keys():
            start_lane_paths[lane_index].sort(key=itemgetter('length'))
            end_lane_paths[lane_index].sort(key=itemgetter('length'))

        self.start_lane_switching = start_lane_paths
        self.end_lane_switching = end_lane_paths

    def get_switching_for_current_lane(self, end_lane_index=None):
        if self.graph is None:
            print "GRAPH isn't calculated"
            return {}
        cur_index = self.cur_info.piece_index
        next_piece = self.w_pieces[cur_index+1]
        #recalculate only if switch is in next piece and some cars are in front
        slowest_cars = getattr(self.cur_info, 'slowest_cars', [])
        if "switch" in next_piece and slowest_cars:
            print "Recalculate switching", cur_index
            self.calculate_all_switching(parameter='time')

        if end_lane_index is None:
            end_lane_index = self.cur_info.end_lane_index
        #print "END_LANE", end_lane_index
        switching_info = self.start_lane_switching[end_lane_index][0]
        #print "Path LENGTH:", switching_info["length"]
        #print "FROM:", switching_info["start"]["lane"]
        #print "TO:", switching_info["end"]["lane"]
        print "SWITCH", switching_info["switching"]
        return switching_info


    @property
    def last_lap(self):
        """Returns true if it is the last lap"""
        # TODO: get last lap based on time reamining time etc..
        if self.qualifying:
            return False
        return self.current_lap == (self.laps - 1)

    def _find_apex(self, turn_indexes):
        length_of_turn = len(turn_indexes)
        stop_of_turn = length_of_turn/2
        if self.trackname == "Keimola" or self.trackname == "Suzuka":
        ##Used for imola
            apex = [(turn_indexes[0], 0.2),
                    (turn_indexes[-1], 0.8)]
            return apex
        if length_of_turn%2 == 0:
            apex = [(turn_indexes[stop_of_turn-1], 0.4),
                    (turn_indexes[stop_of_turn], 0.2)]
        else:
            apex = [(turn_indexes[stop_of_turn], 0.2),
                    (turn_indexes[stop_of_turn], 0.8)]
        return apex

        if length_of_turn%2 == 0:
            if length_of_turn == 2:
                apex = [(turn_indexes[stop_of_turn-1], 0.2),
                        (turn_indexes[stop_of_turn], 0.8)]
            else:
                apex = [(turn_indexes[0], 0.2),
                        (turn_indexes[-1], 0.8)]
        elif length_of_turn > 1:
            apex = [(turn_indexes[0], 0.6),
                    (turn_indexes[-1], 0.5)]
        else:
            apex = [(turn_indexes[stop_of_turn], 0.3),
                    (turn_indexes[stop_of_turn], 0.8)]
        return apex

    def in_a_turn(self):
        return self.cur_apex is not None

    def _next_apex(self):
        cur_index = self.cur_info.piece_index
        self.inside_apex = False
        if self.cur_info.is_this_different_piece():
            self.next_apex = None
            self.cur_apex = None
            for turn_index, turn in enumerate(self.speed_limits):
                if cur_index in turn["index"]:
                    self.cur_apex = turn
                    try:
                        self.next_apex = self.speed_limits[turn_index+1]
                    except Exception:
                        pass
                elif cur_index < min(turn["index"]):
                    self.next_apex = turn
                    break
            if self.last_lap:
                if self.next_apex["index"][0] > len(self.pieces):
                    self.next_apex = None
            #print "index:", cur_index
            #print "CURRENT TURN:", self.cur_apex
            #print "NEXT TURN:", self.next_apex
        # We aren't in a turn
        if self.cur_apex is None:
            self.coming_apex = self.next_apex
        # We are in a turn
        else:
            cur_dist = self.cur_info.in_piece_distance
            apex = self.cur_apex
            # Before or on apex
            apex_start_index, start_factor = apex["apex"][0]
            apex_end_index, end_factor = apex["apex"][1]
            apex_on_one_segm = (apex_start_index == apex_end_index)
            # TODO: Switching?
            cur_lane = self.cur_info.end_lane_index
            apex_start_loc = self.pieces[cur_index]["length"](cur_lane) * start_factor
            apex_end_loc = self.pieces[cur_index]["length"](cur_lane) * end_factor
            # Coming apex is in curent turn
            if cur_index < apex_start_index:
                self.coming_apex = self.cur_apex
                #return
            # Coming apex is in next turn we are past current apex
            elif cur_index > apex_end_index:
                self.coming_apex = self.next_apex
                #return
            elif apex_on_one_segm:
                if apex_start_loc <= cur_dist < apex_end_loc:
                    # Inside current apex
                    self.coming_apex = self.cur_apex
                    #print "INSIDE APEX"
                    self.inside_apex = True
                # Coming into apex
                elif cur_dist < apex_start_loc:
                    self.coming_apex = self.cur_apex
                # Past current apex
                else:
                    self.coming_apex = self.next_apex
            elif not apex_on_one_segm:
                if cur_index == apex_start_index:
                    # Not yet on current apex
                    if cur_dist < apex_start_loc:
                        self.coming_apex = self.cur_apex
                    # Inside current apex
                    else:
                        #print "INSIDE APEX"
                        self.inside_apex = True
                        self.coming_apex = self.cur_apex
                elif cur_index == apex_end_index:
                    # Inside current apex
                    if cur_dist < apex_end_loc:
                        self.coming_apex = self.cur_apex
                        #print "INSIDE APEX"
                        self.inside_apex = True
                    # Past current apex
                    else:
                        self.coming_apex = self.next_apex
                elif apex_start_index < cur_index < apex_end_index:
                    self.coming_apex = self.cur_apex
                    #print "INSIDE APEX"
                    self.inside_apex = True
        #print "Next coming", self.coming_apex


    def distance_to_next_turn(self):
        return self.distance_to_apex(self.next_apex)

    def distance_to_turn_apex(self):
        cur_index = self.cur_info.piece_index

    def distance_to_apex(self, apex):
        if apex is None:
            distance = sys.maxint
            return distance
        if self.inside_apex:
            return 0
        cur_index = self.cur_info.piece_index
        apex_start_index, start_factor = apex["apex"][0]
        if cur_index > apex_start_index:
            apex_start_index, start_factor = apex["apex"][1]
        cur_lane = self.cur_info.end_lane_index
        #print cur_index, apex_start_index
        if cur_index == apex_start_index:
            cur_piece = self.pieces[cur_index]
            if "angle" in cur_piece:
                length = cur_piece["length"](cur_lane)
            else:
                length = cur_piece["length"]
            dist_to_end =  length*start_factor-self.cur_info.in_piece_distance
            #print "Distance", dist_to_end
            return dist_to_end
        dist_to_end = self.cur_info.distance_to_end
        #print "D_TO_E", dist_to_end
        #if cur_index == 53:
            #import ipdb; ipdb.set_trace()
        pieces_list = self.pieces[cur_index+1:apex_start_index+1]
        # Next turn is in the next lap
        if apex_start_index > len(self.pieces):
            pieces_list.extend(self.pieces[0:apex_start_index%len(self.pieces)+1])
        for index, cur_piece in enumerate(pieces_list, cur_index+1):
            if "angle" in cur_piece:
                length = cur_piece["length"](cur_lane)
            else:
                length = cur_piece["length"]
            #print index, length, apex_start_index
            if index == apex_start_index:
                length *= start_factor
            dist_to_end+=length
        #print "Distance", dist_to_end
        return dist_to_end




    def calculate_speed_limits(self, debug=False, F_slip=False,
            mu_friction=0.0459542, gravity=9.8, skip=False):
        if not F_slip:
            if self.mu_friction is None:
                self.mu_friction = mu_friction
            elif not skip:
                if mu_friction > self.mu_friction:
                    print "NEW friction can't be larger then previous on crash"
                    return
        self.gravity = gravity
        speed_limits = []
        current_speed_limit = None
        prev_data = (0,0,0)
        switch_locations = set()
        self.last_turn_index = None
        num_pieces = len(self.pieces)

        for index, piece in enumerate(self.pieces):
            angle = piece.get("angle", None)
            if "switch" in piece:
                switch_locations.add(index)
                switch = " SWITCH"

            else:
                switch = ""

            if angle is not None:
                self.last_turn_index = index
                v_max = math.sqrt(mu_friction * gravity * piece["radius"])
                #HACK: Currently add max speed to previos piece
                # TODO: Add lower speed to previous and higher to current?
                # Add speed to half previous?
                # FIXME: doesn't work if first piece is bend
                # Should also be careful if first piece is bend and second
                # straight. We have to go slow there
                self.pieces[index-1]["speed_limit"] = {}
                piece_speed_limit = self.pieces[index-1]["speed_limit"]
                piece["radius_ng"] = {}
                # Each lane has separate speed limit in curves
                for lane_index, distance_from_center in \
                    self.lane_indexes.iteritems():
                    # We have to add or substract on which lane are we
                    # currently
                    # adding or substracting depends on the direction
                    # of the angle
                    # curve to the right
                    if angle > 0:
                        # negative distances are on the outside of the curve
                        # positive on the inside
                        lane_diff = -1 * distance_from_center
                    # curve to the left
                    else:
                        # positive distances are on the outside of the curve
                        # negative on the inside
                        lane_diff = distance_from_center
                    radius = piece["radius"] + lane_diff
                    piece["radius_ng"][lane_index] = radius
                    if F_slip:
                        v_max_lane = math.sqrt(self.F_slip * radius) #* 1.13
                    else:
                        v_max_lane = math.sqrt(mu_friction * gravity * radius)

                    graph_start = "L_%d_%d" % (lane_index, index)
                    graph_end = "L_%d_%d" % (lane_index, (index + 1) % num_pieces)
                    dist = self.graph[graph_start][graph_end]["weight"]
                    self.graph[graph_start][graph_end]["v_max"] = \
                            v_max_lane

                    piece_speed_limit[lane_index] = v_max_lane
                if "switch" in piece:
                    for start_lane_index, end_lane_index in \
                        pairwise(self.lane_indexes):
                        v_lane = (piece_speed_limit[start_lane_index] +
                                piece_speed_limit[end_lane_index]) / 2.0
                        graph_start = "L_%d_%d" % (start_lane_index, index)
                        graph_end = "L_%d_%d" % (end_lane_index, (index + 1) % num_pieces)
                        dist = self.graph[graph_start][graph_end]["weight"]
                        self.graph[graph_start][graph_end]["v_max"] = \
                                v_lane
                        
                        graph_start = "L_%d_%d" % (end_lane_index, index)
                        graph_end = "L_%d_%d" % (start_lane_index, (index + 1) % num_pieces)
                        self.graph[graph_start][graph_end]["v_max"] = \
                                v_lane
                cur_data = (mu_friction, gravity, piece["radius"])
                if current_speed_limit is not None:
                    if prev_data == cur_data:
                        current_speed_limit["index"].append(index)
                    else:
                        # TODO: Add different limit for lanes
                        current_speed_limit["apex"] = \
                                self._find_apex(current_speed_limit["index"])
                        speed_limits.append(current_speed_limit)
                        current_speed_limit = {'index': [index], 'speed_o': v_max}
                        current_speed_limit["speed"] = piece_speed_limit
                else:
                    current_speed_limit = {'index': [index], 'speed_o': v_max}
                    current_speed_limit["speed"] = piece_speed_limit
                prev_data = cur_data
            else:
                # Straight piece
                v_lane = self.max_speed
                for lane_index in self.lane_indexes.keys():
                    graph_start = "L_%d_%d" % (lane_index, index)
                    graph_end = "L_%d_%d" % (lane_index, (index + 1) % num_pieces)
                    dist = self.graph[graph_start][graph_end]["weight"]
                    self.graph[graph_start][graph_end]["v_max"] = v_lane
                if "switch" in piece:
                    #SWITCHING
                    for start_lane_index, end_lane_index in \
                        pairwise(self.lane_indexes):
                        graph_start = "L_%d_%d" % (start_lane_index, index)
                        graph_end = "L_%d_%d" % (end_lane_index, (index + 1) % num_pieces)
                        dist = self.graph[graph_start][graph_end]["weight"]
                        self.graph[graph_start][graph_end]["v_max"] = \
                                v_lane
                        
                        graph_start = "L_%d_%d" % (end_lane_index, index)
                        graph_end = "L_%d_%d" % (start_lane_index, (index + 1) % num_pieces)
                        self.graph[graph_start][graph_end]["v_max"] = \
                                v_lane
                v_max = ""
                if current_speed_limit is not None:
                    current_speed_limit["apex"] = \
                            self._find_apex(current_speed_limit["index"])
                    speed_limits.append(current_speed_limit)
                    current_speed_limit = None
                #print "V_MAX: ", v_max

            if debug:
                if angle is None:
                    print "    STRAIGHT{0}".format(switch)
                elif angle > 0:
                    print "        >RIGHT TURN{0} VMAX: {1}".format(switch, v_max)
                elif angle < 0:
                    print "<LEFT TURN{0} VMAX: {1}".format(switch, v_max)
        #print speed_limits
        # Added first limit to the end
        speed_limits[-1]["last_corner"] = True
        first_limit = speed_limits[0].copy()
        first_limit["index"] = [x + len(self.pieces) for x in first_limit["index"]]
        first_limit["apex"] = [(x[0] + len(self.pieces), x[1]) for x in
                first_limit["apex"]]
        speed_limits.append(first_limit)
        self.speed_limits = speed_limits
        self.calculate_all_switching(parameter="time")
        print speed_limits

    def lap_finished(self, data):
        if data["car"]["name"] == self.name:
            self.current_lap = data["lapTime"]["lap"] + 1
            return True
        else:
            color = data["car"]["color"]
            self.cars[color].lap_finished(data)
            return False

    def get_current_speed_limit_cars(self, no_limit=11, last_kick=20):
        car_index = self.cur_info.piece_index
        car_lanes = self.cur_info.lane_index
        if self.cur_info.is_this_different_piece():
            nice_perc = []
            speeds = []
            percentages = \
                sorted(self.other_car_speeds[car_index][car_lanes].keys())
            #if not percentages:
                #return 0
            #if percentages[0] != 0:
                #percentages[0] = 0
            #if percentages[-1] != 50:
                #percentages[-1] = 50
            max_speed = 0
            for perc in percentages:
                cars = self.other_car_speeds[car_index][car_lanes][perc]
                for color, speed in cars.iteritems():
                    if speed > max_speed:
                        max_speed = speed
                if max_speed != 0:
                    speeds.append(max_speed)
                    nice_perc.append(perc)
            if not nice_perc:
                return 0
            #print len(nice_perc), len(speeds)
            #new_speeds = interp1d(nice_perc, speeds,
                    #kind='cubic') #(range(50))
            #new_speeds = UnivariateSpline(nice_perc, speeds, k=2)
            #new_speeds = np.interp(range(51), nice_perc, speeds)
            #new_speeds = sp.interpolate.interpn(range(52), nice_perc,
                    #speeds,bounds_error=False, fill_value=None)
            new_speeds = sp.interpolate.splrep(nice_perc, speeds, k=1, s=0)
            self.new_speeds = new_speeds
        else:
            new_speeds = getattr(self, 'new_speeds', None)
        if new_speeds is None:
            return 0
        car_perc = int(round(self.cur_info.perc_dist_piece * 50))
        #print "LEN; P", len(new_speeds), car_perc
        #try:
        return sp.interpolate.splev(car_perc, new_speeds)
        return new_speeds[car_perc]
        #return new_speeds(car_perc)

    def get_current_speed_limit3(self, no_limit=11, last_kick=20):
        car_index = self.cur_info.piece_index
        car_lanes = self.cur_info.lane_index
        car_perc = int(round(self.cur_info.perc_dist_piece * 50))
        cars = self.other_car_speeds[car_index][car_lanes][car_perc]
        max_speed = 0
        #print "GET", car_index, car_lanes, \
            #self.other_car_speeds[car_index][car_lanes]
        #print cars
        #print self.other_car_speeds
        for color, speed in cars.iteritems():
            if speed > max_speed:
                max_speed = speed
        return max_speed


    def get_current_speed_limit(self, no_limit=11, last_kick=20):
        # End of last turn in last lap Pedal to the metal!
        if self.last_lap and self.cur_info.piece_index > self.last_turn_index:
            return last_kick
        # Using other cars for setting speed
        #if self.current_lap > 0:
            #speed = self.get_current_speed_limit_cars()
            #if speed != 0:
                #return speed-0.5
        # TODO: What if we are switching?
        lane_index = self.cur_info.lane_index[1]
        speed_limits = \
                self.pieces[self.cur_info.piece_index].get("speed_limit", None)
        distance_to = self.distance_to_apex(self.coming_apex)
        if self.coming_apex is None:
            return no_limit
        stop_length, ticks = \
            self.distance_to_slowdown(self.coming_apex["speed"][lane_index])

        try:
            if self.next_apex["speed"][lane_index] < \
                self.coming_apex["speed"][lane_index]:
                stop_next_length, ticks1 = \
                        self.distance_to_slowdown(self.next_apex["speed"][lane_index])
                if stop_next_length >= self.distance_to_next_turn():
                    #print "Break for next apex"
                    return self.next_apex["speed"][lane_index]
        except Exception:
            pass
        #print "SL, DI", stop_length, distance_to
        if self.inside_apex or stop_length >= distance_to:
            return self.coming_apex["speed"][lane_index]
        #else:
            #return no_limit
        if speed_limits is None:
            return no_limit
        return speed_limits[lane_index]

    def set_throttle(self, throttle_value):
        #print "Setting throttle"
        if self.is_spawned:
            self.is_crashed = False
            self.is_spawned = True
        if self.turbo_factor is not None:
            self.max_speed = self.original_max_speed * self.turbo_factor
            self.turbo_factor = None
        if not self.turbo:
            self.max_speed = self.original_max_speed
            self.turbo_factor = None
        self.current_throttle = throttle_value

    def crashed(self, data):
        # Somebody else crashed
        if data["name"] != self.name:
            color = data["color"]
            self.cars[color].crashed(data)
        else:
            #print "CRASHED"
            self.cur_info.calc_speed = 0
            self.is_crashed = True
            self.is_spawned = False
            #if self.color in self.cars.keys(): 
                #car_index = self.cur_info.piece_index
                #lane_index = self.cur_info.lane_index
            # Turbo stops in crash
            self.turbo_end(data)

    def spawned(self, data):
        # Somebody else spawned
        if data["name"] != self.name:
            color = data["color"]
            self.cars[color].spawned(data)
        else:
            #print "SPAWNED"
            # Crash is false has to be set in next throttle
            self.is_spawned = True

    def turbo_start(self, data, cur_turbo): #factor, duration):
        if data["name"] == self.name:
            #print "TURBO START"
            factor = cur_turbo["turboFactor"];
            duration = cur_turbo["turboDurationTicks"]
            self.turbo_factor = factor
            self.turbo = True
            return True
        else:
            return False
            # This probably doesn't work yet
            color = data["color"]
            self.cars[color].turbo_start(data, cur_turbo)
            return False

    def turbo_end(self, data):
        if data["name"] == self.name:
            #print "TURBO END"
            self.turbo = False
            return True
        else:
            color = data["color"]
            self.cars[color].turbo_end(data)
            return False

    @staticmethod
    def convert_dist_pos_to_car_dist(cardim, distance):
        new_dist = distance - cardim.guide_flag_pos
        new_dist = new_dist - cardim.car_dist_to_end
        return new_dist

    def calc_distance_to_car(self, color):
        car_dash = self.cars[color]
        # Ignore DNFed  and crashed car
        if car_dash.has_dnf or car_dash.has_finished or car_dash.is_crashed:
            return sys.maxint
        other_index = car_dash.cur_info.piece_index
        other_car_loc = car_dash.cur_info.in_piece_distance
        cur_car_loc = self.cur_info.in_piece_distance
        cur_index = self.cur_info.piece_index
        piece_idx_dist = (other_index - cur_index) % self.num_pieces
        dist_to_end = self.cur_info.distance_to_end
        if other_index == cur_index:
            if other_car_loc > cur_car_loc:
                # Distance between cars
                distance = other_car_loc - cur_car_loc
                return self.convert_dist_pos_to_car_dist(self.car_dim,
                        distance)
            # Car is behind us
            else:
                return sys.maxint
        # Distance between cars is less than 4 pieces
        elif 0 < piece_idx_dist <=4:
            cur_lane = self.cur_info.end_lane_index
            pieces_list = self.w_pieces[cur_index+1:other_index]
            distance = self.cur_info.distance_to_end
            for track_piece in pieces_list:
                if "angle" in track_piece:
                    length = track_piece["length"](cur_lane)
                else:
                    length = track_piece["length"]
                distance += length
            distance += other_car_loc
            return self.convert_dist_pos_to_car_dist(self.car_dim,
                    distance)

        # Distance between cars is more than 4 track pieces
        # return big distance
        else:
            return sys.maxint


    def cars_to_overtake(self, colors, tick):
        # Removes closest slowest cars in previous tick
        prev_slowest_cars = getattr(self.prev_info, 'slowest_cars', {})
        for prev_car in prev_slowest_cars:
            graph_start, graph_end = prev_car.split("+")
            #print "Remove prev car", prev_car 
            del self.graph[graph_start][graph_end]["car_speed"]
        slowest_cars = {}
        for color in colors:
            car_data = self.cars[color]
            car_index = car_data.cur_info.piece_index
            car_speed = car_data.cur_info.speed
            lane_index = car_data.cur_info.lane_index
            graph_start = "L_%d_%d" % (lane_index[0], car_index)
            graph_end = "L_%d_%d" % (lane_index[1], (car_index + 1) % self.num_pieces)
            slowest_car = slowest_cars.get(graph_start+"+"+graph_end, None)
            # No car on this car location
            if slowest_car is None:
                slowest_cars[graph_start+"+"+graph_end] = car_speed
            # Car on this location is faster then current car
            elif slowest_car > car_speed:
                slowest_cars[graph_start+"+"+graph_end] = car_speed

        for graph_start_end, car_speed in slowest_cars.iteritems():
            graph_start, graph_end = graph_start_end.split("+")
            #car_graph_data = self.graph[graph_start][graph_end].get("car_data", None)
            #if car_graph_data is None:
                #self.graph[graph_start][graph_end]["car_data"] = {}
            self.graph[graph_start][graph_end]["car_speed"] = car_speed
            #print "ADD NEW SLOW", graph_start_end
        #print colors
        #print "____"
        self.cur_info.slowest_cars = slowest_cars.keys()

    def update_other_speeds(self):
        # Updates max speeds of other cars
        for color, car_dash in self.cars.iteritems():
            car_speed = car_dash.cur_info.speed
            car_index = car_dash.cur_info.piece_index
            car_perc = int(round(car_dash.cur_info.perc_dist_piece * 50))
            car_lanes = car_dash.cur_info.lane_index
            #print color, car_speed, car_index, car_perc, \
                #car_dash.cur_info.perc_dist_piece
            this_part = \
                self.other_car_speeds[car_index][car_lanes][car_perc] \
                .get(color,0)
            #print this_part
            if this_part < car_speed and \
                (car_speed - car_dash.max_acc()) <= 0.4:
                self.other_car_speeds[car_index][car_lanes][car_perc][color] = car_speed
                #print "ADDED", car_index, car_lanes, car_perc, color, car_speed
            #elif (car_speed - car_dash.max_acc()) > 0.4:
                #print "SPEED, ", car_speed, ">", car_dash.max_acc(), \
                    #car_speed-car_dash.max_acc()
                #print "Missed", car_index, car_lanes, car_perc, color, car_speed
        #print self.other_car_speeds


    def new_car_position(self, datas, tick, logtype=LogType.GAME):
        data = None
        for car_data in datas:
            if car_data["id"]["name"] == self.name:
                data = car_data
            elif not self.dummy:
                self.cars[car_data["id"]["color"]].new_car_position(datas,
                        tick, logtype)
                #break
        if data is None:
            print "Car {0} missing in car position?!?. Madness I Tell you!"
            return
        if tick is not None:
            self.prev_info = self.cur_info
            self.prev_info.throttle = self.current_throttle
        else:
            self.prev_info = None
        piece_pos = data["piecePosition"]
        in_piece_distance = piece_pos["inPieceDistance"]
        lane_data = piece_pos["lane"]
        start_lane_index = lane_data["startLaneIndex"]
        end_lane_index = lane_data["endLaneIndex"]
        angle = data["angle"]
        # In visualization json angle is direction and angleOffset is slip Angle
        if logtype == LogType.VIZUALIZATION:
            angle = data["angleOffset"]
        self.cur_info = CarInfo(self.prev_info, self.pieces, self.lane_indexes,
                tick, piece_pos["pieceIndex"], in_piece_distance, start_lane_index,
                end_lane_index, angle, crashed=self.is_crashed,
                max_speed=self.max_speed, acc_factor=self.acc_factor,
                acc_calc=self.acc_factor_calculated,
                max_calc=self.max_speed_calculated) 
        if self.dummy:
            return
        if tick is not None:
            #self.update_other_speeds()
            cars_to_overtake = []
            for color in self.cars.keys():
                distance = self.calc_distance_to_car(color)
                prev_distance, prev_tick = self.cars[color].distance_to_car
                getting_closer = 0
                if prev_distance != sys.maxint and (tick-prev_tick) == 1:
                    if prev_distance >= distance:
                        speed_ratio = 0
                        try:
                            speed_ratio = self.cars[color].cur_info.speed/self.cur_info.speed
                        except ZeroDivisionError:
                            pass
                        print "We are getting closer to ", self.cars[color].name, \
                            distance, tick, speed_ratio, \
                                self.cars[color].getting_closer_x_ticks 
                        getting_closer = self.cars[color].getting_closer_x_ticks + 1
                        if (self.cars[color].getting_closer_x_ticks >= 20 or 
                            distance < 200) and speed_ratio < 1:
                            # We need to overtake
                            cars_to_overtake.append(color)
#TODO: remove from overtake only when they are more than 20 away, if they were
#                            less then 200
                    #else:
                        #print "not closer to ", self.cars[color].name, \
                            #distance, tick, \
                                #self.cars[color].getting_closer_x_ticks 
                #else:
                    #print "not ", self.cars[color].name, \
                        #distance, tick, \
                            #self.cars[color].getting_closer_x_ticks 
                self.cars[color].distance_to_car = (distance, tick)
                self.cars[color].getting_closer_x_ticks = getting_closer
            self.cars_to_overtake(cars_to_overtake, tick)

        if self.cur_info.acc_factor is not None:
            self.acc_factor = self.cur_info.acc_factor
            self.acc_factor_calculated = True
            print self.acc_max_speed
            print "MAX_SPEED?", self.acc_max_speed/self.acc_factor
        if self.cur_info.max_speed is not None:
            self.max_speed = self.cur_info.max_speed
            self.original_max_speed = self.max_speed
            self.max_speed_calculated = True
            #for car_dash in self.cars.values():
                #car_dash.acc_factor = self.acc_factor
                #car_dash.acc_factor_calculated = True
                #car_dash.max_speed = self.max_speed
                #car_dash.original_max_speed = self.max_speed
                #car_dash.max_speed_calculated = True
        # TODO: Wrong when switching
        self.path_driven += self.cur_info.driven_in_tick
        self._next_apex()
        if tick == 1:
            self.acc_max_speed = self.cur_info.driven_in_tick
        if angle != 0 and self.F_slip is None:
            try:
                if self.cur_info.speed > 15:
                    print "SPEED TOO HIGH"
                    #return
                print "Angle, tick", angle, tick
                print "Calculated F_slip"
                # FIXME: Exception division by zero dobo tu
                B = math.degrees(self.cur_info.calc_speed/self.radius)
                B_slip = B - angle
                V_tresh = math.radians(B_slip) * self.radius
                self.F_slip = math.pow(V_tresh, 2.0) / self.radius
                #self.calculate_speed_limits(F_slip=True)
                max_spd = math.sqrt(self.F_slip * self.radius) 
                mu_friction = math.pow(max_spd, 2.0) / \
                        (9.8 * self.radius)
                print "MU:", mu_friction
                #self.calculate_speed_limits(mu_friction=mu_friction, skip=True)
            except ZeroDivisionError:
                pass

    def dnf(self, data):
        car_color = data["car"]["color"]
        self.cars[car_color].has_dnf = True

    def finish(self, data):
        if data["name"] != self.name:
            car_color = data["color"]
            self.cars[car_color].has_finished = True

    def max_acc(self):
        prev_speed = self.prev_info.speed
        throttle_for_speed = 1
        new_speed = prev_speed + ((throttle_for_speed *
            self.original_max_speed) - prev_speed) * self.acc_factor
        return new_speed

    def distance_to_slowdown(self, wanted_speed):
        cur_speed = self.cur_info.speed
        distance = 0
        ticks = 0
        throttle_for_speed = 0
        prev_speed = cur_speed
        new_speed = cur_speed
        if wanted_speed > new_speed:
            return 0, 0
        #print "SPEED", new_speed, wanted_speed
        while new_speed > wanted_speed:
            new_speed = prev_speed + ((throttle_for_speed *
                self.max_speed) - prev_speed) * self.acc_factor
            distance+=prev_speed
            ticks += 1
            prev_speed=new_speed
        return distance, ticks

    def print_info(self):
        print " tick: {0} pindex:{1}, pdistance:{2}, angle:{3}, speed:{5}," \
                "length: {6}, v_speed: {10}, accel: {7}" \
        .format(self.cur_info.game_tick,
                self.cur_info.piece_index,
                self.cur_info.in_piece_distance,
                self.cur_info.slip_angle,
                self.cur_info.piece_index,
                self.cur_info.speed,
                self.cur_info.length,
                self.cur_info.accel,
                self.cur_info.speed_y,
                self.cur_info.speed_x,
                self.cur_info.v_speed,
                )

    @property
    def is_cur_piece_curved(self):
        """Returns true if current piece is curved"""
        return "radius" in self.pieces[self.cur_info.piece_index]

    @property
    def radius(self):
        """Gets radius of current piece"""
        # TODO: Switching
        end_lane_index = self.cur_info.end_lane_index
        cur_piece = self.pieces[self.cur_info.piece_index]
        if "radius" in cur_piece:
            return cur_piece["radius_ng"][end_lane_index] 
        else:
            return 0

    @property
    def curve_angle(self):
        """Gets angle of a bend"""
        return self.pieces[self.cur_info.piece_index].get("angle", 0)
    
    @property
    def prev_curve_angle(self):
        """Gets angle of a previous bend"""
        return self.pieces[self.cur_info.piece_index - 1].get("angle", 0)

class CarInfo(object):

    def __init__(self, prev_info, pieces, lane_indexes, game_tick, piece_index=0, in_piece_distance=0.0, start_lane_index=0,
            end_lane_index=0, slip_angle=0.0, acc_factor=0.02, max_speed=10.0,
            acc_calc=False, max_calc=False, crashed=False):
        self.TICK_LENGTH = 1.0 #/60.
        self.game_tick = game_tick
        self.piece_index = piece_index
        self.in_piece_distance = in_piece_distance
        self.start_lane_index = start_lane_index
        self.end_lane_index = end_lane_index
        self.lane_index = (start_lane_index, end_lane_index)
        self.slip_angle = slip_angle
        self.acc_factor = None
        self.max_speed = None
        cur_piece = pieces[self.piece_index]
        # TODO: We have to figure out acc_factor and max speed. They could change
        # We aren't switching here
        if self.start_lane_index == self.end_lane_index:
            # Curved piece
            if "angle" in cur_piece:
                #Distance in curved parts depends on which lane are we
                #driving
                self.length = cur_piece["length"](start_lane_index)
            # Normal straight piece
            else:
                self.length = cur_piece["length"]
        # We are switching on these piece
        else:
            # Normal straight piece
            if "angle" not in cur_piece:
                # It is a little longer than that apparently
                #switch_height = math.fabs(lane_indexes[start_lane_index]) \
                    #+ math.fabs(lane_indexes[end_lane_index])
                #import ipdb; ipdb.set_trace()
                #self.length = math.sqrt(math.pow(cur_piece["length"], 2.0) +
                        #math.pow(switch_height, 2.0))
                # number is True switch length (102.7)/calculated length
                # Straight line switch bend on length 100 is measured:  102.060272785287
                self.length = cur_piece["length"] *  1.02060272785287 

            # Curved piece
            else:
                start_length = cur_piece["length"](start_lane_index)
                end_length = cur_piece["length"](end_lane_index)

                #radius_diff = math.fabs(start_length - end_length)
                #n_turns = math.fabs(pieces[piece_pos["pieceIndex"]]["angle"])/360.

                #print radius_diff, n_turns

                bigger_length = max(start_length, end_length)


                #Calculates distance based on spiral length formula:
                # $$\sqrt{(\pi*R*2*n\_turns)^2 + (radius\_diff * n\_turns)^2}$$
                #self.length = math.sqrt(math.pow((math.pi * bigger_length * 2 * n_turns), 2.0) +
                        #math.pow((radius_diff * n_turns), 2.0))

                #print "GL: ", length
                #print "AL: ", (start_length+end_length)/2.


                #self.length = (start_length+end_length)/2.
                # Kemola length is 81.054652204908 first bend switch from
                # inside to outside
                # length = outside length * outside/length/81.054
                # It works in keimola and USA? but breaks down on Germany
                self.length = bigger_length *  0.9828760806865157
                #print start_length, end_length
        if prev_info is None:
            self.speed = 0
            self.v_speed = 0
            self.slip_angle = 0
            self.calc_speed = 0
            self.throttle = 0.0
            self.ang_speed = 0.0
            self.driven_in_tick = 0.0
            self.perc_dist_piece = 0.0
            return

        self.perc_dist_piece = self.in_piece_distance/self.length

        # Driving forvard in the same piece
        if self.piece_index == prev_info.piece_index:
            self.driven_in_tick = self.in_piece_distance - \
                    prev_info.in_piece_distance
            self.different_piece = False
        else:
            self.different_piece = True
            distance_in_prev = prev_info.length-prev_info.in_piece_distance
            self.driven_in_tick = distance_in_prev + self.in_piece_distance
        self.speed = self.driven_in_tick/self.TICK_LENGTH
        self.v_speed = self.speed - prev_info.speed
        if crashed:
            self.calc_speed = 0
        else:
            try:
                throttle_for_speed = prev_info.throttle
            except AttributeError:
                #print "Throttle MISSING!", self.game_tick, self.piece_index
                throttle_for_speed = 0
            self.calc_speed = prev_info.calc_speed + ((throttle_for_speed *
                max_speed) - prev_info.calc_speed) * acc_factor
            if throttle_for_speed == 0:
                if not acc_calc:
                    acc_factor1 = - (self.speed - prev_info.speed)/prev_info.speed
                    self.acc_factor = acc_factor1
                    print "ACC_FACTOR:", acc_factor1, self.game_tick
            elif acc_calc and not max_calc:
                m_speed = (self.speed +(acc_factor - 1) * prev_info.speed) / \
                    (throttle_for_speed * acc_factor)
                self.max_speed = m_speed
                self.calc_speed = self.speed
                print "MAX_SPEED:", m_speed, self.game_tick
        if self.v_speed != 0:
            self.accel = self.v_speed / self.TICK_LENGTH
        else:
            self.accel = 0
        if "angle" in cur_piece:
            # TODO: Which angle? Slip angle or angle
            self.speed_y = cur_piece["sin_rad_angle"] * self.speed
            self.speed_x = cur_piece["cos_rad_angle"] * self.speed
            # TODO: Switching?
            self.ang_speed = (self.speed / 
                cur_piece["radius_ng"][end_lane_index])
        else:
            self.ang_speed = 0
            self.speed_y = self.speed_x = 0

    @property
    def distance_to_end(self):
        return self.length - self.in_piece_distance

    def is_this_different_piece(self):
        # game_tick can be None
        # GameTick 1 is first gameTick in carPositions
        if self.game_tick == 1 or self.game_tick is None:
            return True
        else:
            return self.different_piece


